#!/usr/bin/env bash

set -euox pipefail

CLOUDFLARED_VERSION="2022.4.0"
IMAGE_BASE=stavros/cloudflared
IMAGE_NAME=$IMAGE_BASE:$CLOUDFLARED_VERSION

sed "s/CLOUDFLARED_VERSION/$CLOUDFLARED_VERSION/g" Dockerfile.template > Dockerfile

echo "Building $IMAGE_NAME..."
docker build -t "$IMAGE_NAME" -t "$IMAGE_BASE:latest" .

echo "Pushing $IMAGE_NAME..."
docker push "$IMAGE_NAME"
docker push "$IMAGE_BASE:latest"
