#!/usr/bin/env sh

if [ ! -f "/root/.cloudflared/tunnel.json" ]; then
    echo "$TUNNEL_JSON" > "/root/.cloudflared/tunnel.json"
fi

cloudflared tunnel --url "$URL" run --credentials-file /root/.cloudflared/tunnel.json $TUNNEL_NAME
