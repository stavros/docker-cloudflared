# docker-cloudflared

This is a Docker image containing
[cloudflared](https://github.com/cloudflare/cloudflared). I needed this because I need
a way to deploy cloudflared that allows for everything being environment variables, for
use with [Harbormaster](https://gitlab.com/stavros/harbormaster).

Try it out with:

```
docker pull stavros/cloudflared
```

To run it, you need to create your tunnel following [the official guide](https://developers.cloudflare.com/cloudflare-one/connections/connect-apps/install-and-setup/tunnel-guide/), basically:

```bash
$ cloudflared tunnel login
$ cloudflared tunnel create <name>
$ cloudflared tunnel route dns <tunnel UUID> <tunnel hostname>
```

This will generate a directory called `~/.cloudflared/` containing a `cert.pem` (you
only need this if you want to manage containers) and a `<tunneluuid>.json` file, whose
contents you need to run the tunnel.

Once you have the above, you need to define a few variables for this container:

* **TUNNEL_NAME**: The name (or UUID) of the tunnel in question.
* **TUNNEL_JSON**: The *contents* (not the path) of the <tunneluuid>.json file.
* **URL**: The URL to route in the tunnel (ie the service cloudflared should talk to), e.g. `https://myservice:5000/`.

You can find the Docker image at:

https://hub.docker.com/r/stavros/cloudflared
